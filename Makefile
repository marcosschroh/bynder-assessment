DUPLICATE_FOLDER=target
AMOUNT=1
PERCENTAGE=1
NO_DUPLICATES_TEST_FOLDER=tests/targets/no-duplicates
HALF_DUPLICATES_TEST_FOLDER=tests/targets/half-duplicates
FULL_DUPLICATES_TEST_FOLDER=tests/targets/full-duplicates

create-duplicates-folder:
	mkdir ${DUPLICATE_FOLDER}

create-duplicates:
	./scripts/create_files.sh ${DUPLICATE_FOLDER} ${AMOUNT} ${PERCENTAGE}

find-duplicates:
	python -m duplicates ${DUPLICATE_FOLDER}

clean:
	rm -rf ${DUPLICATE_FOLDER}

build: create-duplicates-folder create-duplicates

init: clean build

# Tests
build-tests-folders:
	mkdir ${NO_DUPLICATES_TEST_FOLDER} && mkdir ${HALF_DUPLICATES_TEST_FOLDER} && mkdir ${FULL_DUPLICATES_TEST_FOLDER}

clean-tests-folders:
	rm -rf tests/targets/*

build-test-files:
	./scripts/create_files.sh ${NO_DUPLICATES_TEST_FOLDER} 2 0 && ./scripts/create_files.sh ${HALF_DUPLICATES_TEST_FOLDER} 2 50 && ./scripts/create_files.sh ${FULL_DUPLICATES_TEST_FOLDER} 2 100

run-tests:
	python -m unittest discover -s tests/

tests: build-tests-folders build-test-files run-tests clean-tests-folders