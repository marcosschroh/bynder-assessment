
# Bynder Assessment

Goal: Find duplicates files in a folder

## Features

- [x] Auto create duplicates files based on AMOUNT and PERCENTAGE parameters
- [x] Find duplicates files based on hash

## Requirements

```
python 2/3
```

# Usage

| Command | Description | Default | Example |
| --- | --- | --- | --- |
| `make init $DUPLICATE_FOLDER $AMOUNT $PERCENTEGE` | Create repeated files in DUPLICATE_FOLDER based on $AMOUNT and $PERCENTEGE | DUPLICATE_FOLDER=target AMOUNT=1 PERCENTEGE=1| make init DUPLICATE_FOLDER=juan AMOUNT=10 PERCENTAGE=100 |
| `make clean $DUPLICATE_FOLDER` | Remove files from $DUPLICATE_FOLDER | DUPLICATE_FOLDER=target | |
| `make find-duplicates $DUPLICATE_FOLDER` | Find duplicates files in $DUPLICATE_FOLDER | DUPLICATE_FOLDER=target | |
| `make tests` | Run tests | | |
