import os
import unittest

from duplicates.explorer import FolderExplorer

ROOT_TEST = os.path.dirname(os.path.realpath(__file__))


class ExplorerTest(unittest.TestCase):

    def setUp(self):
        self.explorer = FolderExplorer()

    def test_no_duplicates(self):
        duplicates = self.explorer.find_duplicates(
            os.path.join(ROOT_TEST, 'targets/no-duplicates'))
        self.assertEquals(duplicates, dict())

    def test_all_duplicates(self):
        duplicates = self.explorer.find_duplicates(
            os.path.join(ROOT_TEST, 'targets/half-duplicates'))
        self.assertEquals(len(duplicates.keys()), 1)

    def test_half_duplicates(self):
        duplicates = self.explorer.find_duplicates(
            os.path.join(ROOT_TEST, 'targets/full-duplicates'))
        self.assertEquals(len(duplicates.keys()), 2)


if __name__ == '__main__':
    unittest.main()
