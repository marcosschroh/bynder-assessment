import os
import hashlib
import logging
from collections import defaultdict

logger = logging.getLogger(__name__)


class FolderExplorer:

    @staticmethod
    def hashfile(path, blocksize=65536):
        afile = open(path, 'rb')
        hasher = hashlib.md5()
        buf = afile.read(blocksize)

        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(blocksize)
        afile.close()

        return hasher.hexdigest()

    def show_results(self, duplicates):
        if duplicates:
            template = self.template
            print(template.format('HASH', 'FILES'))

            for file_hash, file_list in duplicates.items():
                if 1 < len(file_list):
                    print(template.format(file_hash, file_list))
            else:
                print('\n')

            msg = 'Found {0} duplicates files. \n'.format(len(duplicates.keys()))
            logger.info(msg)

            return

        logger.info('No duplicate files found. \n')

    @property
    def template(self):
        return "{0:40}|{1:100}"

    def find_duplicates(self, folder):
        logger.info('Exploring folder {0} \n'.format(folder))

        duplicates = defaultdict(list)

        for dir_name, _, file_list in os.walk(folder):
            for f in file_list:
                file_path = os.path.join(dir_name, f)
                file_hash = self.hashfile(file_path)
                duplicates[file_hash].append(file_path)

        duplicates = {k: v for k, v in duplicates.items() if 1 < len(v)}

        return duplicates
