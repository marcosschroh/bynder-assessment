import os
import argparse
import logging.config
import logging
from explorer import FolderExplorer

from config import LOGGING


def is_dir(dirname):
    """Checks if a path is an actual directory"""
    if not os.path.isdir(dirname):
        msg = "{0} is not a directory".format(dirname)
        raise argparse.ArgumentTypeError(msg)
    else:
        return dirname


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "folder",
        help="folder to be explored to find duplicates files",
        type=is_dir
    )

    return parser.parse_args()


def main():
    logging.config.dictConfig(LOGGING)
    args = get_args()
    folder = args.folder

    if os.path.exists(folder):
        explorer = FolderExplorer()
        duplicates = explorer.find_duplicates(folder)
        explorer.show_results(duplicates)


if __name__ == '__main__':
    main()
