import os
import hashlib
import argparse
from collections import defaultdict

parser = argparse.ArgumentParser()
parser.add_argument("folder", help="folder to explore", type=str)


def find_duplicates(folder):
    print('Exploring folder {0}'.format(folder))
    duplicates = defaultdict(list)

    for dir_name, _, file_list in os.walk(folder):
        for f in file_list:
            file_path = os.path.join(dir_name, f)
            file_hash = hashfile(file_path)

            duplicates[file_hash].append(file_path)

    return duplicates


def hashfile(path, blocksize=65536):
    afile = open(path, 'rb')
    hasher = hashlib.md5()
    buf = afile.read(blocksize)

    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    afile.close()

    return hasher.hexdigest()


def show_results(duplicates):
    not_duplicates = True

    for file_hash, file_list in duplicates.items():
        if 1 < len(file_list):
            not_duplicates = False
            print(file_hash, file_list)

    if not_duplicates:
        print('No duplicate files found.')


if __name__ == '__main__':
    args = parser.parse_args()
    folder = args.folder

    if os.path.exists(folder):
        duplicates = find_duplicates(folder)
        show_results(duplicates)
    else:
        print('%s is not a valid path, please verify' % folder)
